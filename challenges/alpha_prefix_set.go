package main

import "fmt"

func main() {
	arrInt := []int{2, 2, 1, 0, 1}
	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", occurIdx(arrInt))
}

func occurIdx(A []int) (res int) {
	n := len(A)
	newArr := make([]bool, n)

	for i := range A {
		if newArr[A[i]] == false {
			newArr[A[i]] = true
			res = i
		}
	}

	return
}
