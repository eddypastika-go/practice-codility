package main

import "fmt"

func main() {
	triangle3(8)
}

func triangle3(n int) {
	//if n%2 == 0 {
	//	panic("must be odd number")
	//}

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if (i-j == 0 && i <= n/2 && i > 0) || (i+j == n && i <= n/2) || i == 0 {
				fmt.Print("*")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}
