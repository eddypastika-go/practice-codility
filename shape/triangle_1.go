package main

import "fmt"

func main() {
	triangle1(4)
}

//*
//**
//***
//****
func triangle1(n int) {
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if i-j > 0 || i == j {
				fmt.Print("*")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}
