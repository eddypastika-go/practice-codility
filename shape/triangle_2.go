package main

import "fmt"

func main() {
	triangle2(11)
}

func triangle2(n int) {
	if n%2 == 0 {
		panic("must be odd number")
	}

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if i+j == n/2 || i == n/2 || i-j == -n/2 {
				fmt.Print("*")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}
