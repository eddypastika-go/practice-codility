package main

import "fmt"

func main() {
	arrInt := []int{1, 1, 1, 3, 9, 9, 8, 3, 8}

	if len(arrInt)%2 == 0 {
		fmt.Println("result: ", 0)
	}

	fmt.Println("input: ", arrInt)
	fmt.Println("result 2: ", findUnpaired2(arrInt))
	fmt.Println("result: ", findUnpairedRecursive(arrInt))
}

func findUnpairedRecursive(arr []int) int {
	if len(arr) == 1 {
		return arr[0]
	}

	val := arr[0]
	for i := 1; i < len(arr); i++ {
		if arr[i] == val {
			arr[i] = arr[len(arr)-1]
			arr = arr[1:]
			break
		}
	}

	arr = arr[:len(arr)-1]

	return findUnpairedRecursive(arr)
}

func findUnpaired2(arr []int) (res int) { // use this for good performance
	timesMap := make(map[int]int)
	for _, value := range arr {
		_, ok := timesMap[value]
		if ok {
			timesMap[value] += 1
		} else {
			timesMap[value] = 1
		}
	}

	for k, v := range timesMap {
		if v%2 != 0 {
			res = k
			break
		}
	}

	return
}
