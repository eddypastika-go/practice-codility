package main

import "fmt"

func main() {
	arrInt := []int{1, 2, 3, 4}
	k := 1

	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", shitArr(arrInt, k))
}

func shitArr(arr []int, k int) []int {
	if k == 0 || len(arr) == 0 {
		return arr
	}

	newArr := make([]int, len(arr))
	newArr[0] = arr[len(arr)-1]
	for i := 1; i < len(arr); i++ {
		newArr[i] = arr[i-1]
	}

	return shitArr(newArr, k-1)
}
