package main

import (
	"fmt"
	"sort"
)

func main() {
	arrInt := []int{10, 2, 5}
	arrInt2 := []int{10, 50, 5, 1}
	i := copy(arrInt, arrInt2)
	fmt.Println("haha: ",i)
	fmt.Println("input: ", arrInt)
	fmt.Println("input: ", arrInt2)
	fmt.Println("result: ", findTriangle(arrInt2))

	y := 400
	p := &y
	*p = 123
	fmt.Println(y)
	fmt.Println(cap(arrInt2[1:4]))
}

func findTriangle(A []int) int {
	sort.Ints(A)
	p, q, r := 0, 0, 0
	for i := 0; i < len(A)-2; i++ {
		p, q, r = A[i], A[i+1], A[i+2]
		if isTriangle(p, q, r) {
			return 1
		}

	}

	return 0
}

func isTriangle(p, q, r int) bool {
	if p+q > r && q+r > p && p+r > q {
		return true
	}

	return false
}
