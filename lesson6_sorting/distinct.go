package main

import (
	"fmt"
	"sort"
)

func main() {
	arrInt := []int{2, 2, 1, 3, 1, 1, 3, 4}
	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", findDistinct(arrInt))
}

func findDistinct(A []int) int {
	if len(A) == 0 {
		return 0
	}

	sort.Ints(A)

	res := 1
	for i := 1; i < len(A); i++ {
		if A[i] != A[i-1] {
			res++
		}
	}

	return res
}
