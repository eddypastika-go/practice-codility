package main

import (
	"fmt"
	"sort"
)

func main() {
	arrInt := []int{-3, 1, 2, -2, 5, 6}
	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", findMax(arrInt))

}

func findMax(A []int) int {
	lenArr := len(A)
	sort.Ints(A)
	max := A[lenArr-1] * A[lenArr-2] * A[lenArr-3]

	if A[0] < 0 && A[1] < 0 { // check - * -
		tmp := A[0] * A[1] * A[lenArr-1]
		if tmp > max {
			max = tmp
		}
	}

	return max
}
