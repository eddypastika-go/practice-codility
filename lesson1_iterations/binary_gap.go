package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	bin := convertToBinary(1041)
	fmt.Println("input: ", bin)
	fmt.Println("result", findGap(bin))
	fmt.Println("result 2:", findGap2(strings.Split(bin, "")))
}

func convertToBinary(n int) string {
	return strconv.FormatInt(int64(n), 2)
}

func findGap(b string) (res int) {
	listBinary := strings.Split(b, "")

	gapArr := make([]int, 0)
	countGap := 0
	startCount := false
	for i, s := range listBinary {
		if s == "1" {
			startCount = !startCount
			if i == 0 {
				continue
			}
		}

		if startCount {
			countGap++
		} else {
			gapArr = append(gapArr, countGap)
			countGap = 0

			if i < len(listBinary) {
				startCount = true
			}
		}
	}

	res = findMaxLength(gapArr)

	return
}

func findMaxLength(arr []int) (res int) {
	res = 0
	for _, n := range arr {
		if res < n {
			res = n
		}
	}

	return
}

func findGap2(bArr []string) int { // 10000010001
	max := 0
	prevIdx := 0
	for i, s := range bArr {
		if s == "1" {
			if i-prevIdx-1 > max {
				max = i - prevIdx - 1
			}
			prevIdx = i
		}
	}

	return max
}

//func calMaxLength(arr []int) (res int) {
//	res = 0
//	for i := 0; i < len(arr)-1; i++ {
//		val := arr[i+1] - arr[i] - 1
//		if val > res {
//			res = val
//		}
//	}
//
//	return
//}
