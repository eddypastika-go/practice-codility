package main

import "fmt"

func main()  {
	in := []int{2,7,11,15}
	target := 9
	fmt.Println("input: ", in, target)
	fmt.Println("result: ", twoSum(in, target))
}

func twoSum(nums []int, target int) []int {
	tMap := make(map[int]int)

	for i, v := range nums {
		tmp := target-v
		_,ok := tMap[v]
		if ok {
			return []int{tMap[v], i}
		} else {
			tMap[tmp] = i
		}
	}

	return []int{}

}
