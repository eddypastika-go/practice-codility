package main

import "fmt"

func main() {
	in := "IX"
	fmt.Println("input: ", in)
	fmt.Println("result: ", romanToInt(in))
}

var mapRoman = map[string]int{
	"I": 1,
	"V": 5,
	"X": 10,
	"L": 50,
	"C": 100,
	"D": 500,
	"M": 1000,
}

func romanToInt(s string) int {
	var res int

	for i, v := range s {
		if i <= len(s)-2 && mapRoman[s[i:i+1]] < mapRoman[s[i+1:i+2]] {
			res -= mapRoman[string(v)]
		} else {
			res += mapRoman[string(v)]
		}
	}

	return res
}
