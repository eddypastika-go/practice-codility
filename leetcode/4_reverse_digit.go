package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	x := 210
	fmt.Println("input: ", x)
	fmt.Println("result: ", reverse(x))

}

func reverse(x int) int {
	var (
		res        int
		isNegative bool
	)

	if x < 0 {
		isNegative = true
		x = x * -1
	}

	s := fmt.Sprintf("%v", x)
	arrTemp := strings.Split(s, "")

	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		arrTemp[i], arrTemp[j] = arrTemp[j], arrTemp[i]
	}

	s = strings.Join(arrTemp, "")
	res, _ = strconv.Atoi(s)

	if isNegative {
		res = res * -1
	}

	if res >= 2147483647 || res <= -2147483648 {
		return 0
	}

	return res
}
