package main

import (
	"fmt"
	"strings"
)

func main() {
	x := -101
	fmt.Println("input: ", x)
	fmt.Println("result: ", isPalindrome(x))

}

func isPalindrome(x int) bool {
	s := fmt.Sprintf("%v", x)
	arrTemp := strings.Split(s, "")

	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		arrTemp[i], arrTemp[j] = arrTemp[j], arrTemp[i]
	}

	 if strings.Join(arrTemp, "") == s {
	 	return true
	 }

	return false
}
