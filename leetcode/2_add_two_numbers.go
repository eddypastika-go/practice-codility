package main

import (
	"fmt"
)

func main() {
	l1 := &ListNode{
		Val: 2,
		Next: &ListNode{
			Val: 4,
			Next: &ListNode{
				Val:  3,
				Next: nil,
			},
		},
	}

	l2 := &ListNode{
		Val: 5,
		Next: &ListNode{
			Val: 6,
			Next: &ListNode{
				Val:  4,
				Next: nil,
			},
		},
	}

	fmt.Println("input: ", l1, l2)
	resp := addTwoNumbers(l1, l2)
	fmt.Println("result: ", resp.Val)
	fmt.Println("result: ", resp.Next.Val)
	fmt.Println("result: ", resp.Next.Next)
}

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	head := &ListNode{}
	for prev, sum := head, 0; l1 != nil || l2 != nil || sum > 0; sum /= 10 {
		if l1 != nil {
			sum += l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			sum += l2.Val
			l2 = l2.Next
		}

		prev.Next = &ListNode{
			Val: sum % 10,
		}
		prev = prev.Next
	}
	return head.Next
}

//func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
//	var res ListNode
//
//	s1 := make([]string, 0)
//	s2 := make([]string, 0)
//
//	fmt.Println(*l1.Next.Next)
//	for l1 != nil {
//		s1 = append(s1, fmt.Sprintf("%v", l1.Val))
//		l1 = l1.Next
//	}
//
//	for l2 != nil {
//		s2 = append(s2, fmt.Sprintf("%v", l2.Val))
//		l2 = l2.Next
//	}
//
//	for i, j := 0, len(s1)-1; i < j; i, j = i+1, j-1 {
//		s1[i], s1[j] = s1[j], s1[i]
//		s2[i], s2[j] = s2[j], s2[i]
//	}
//
//	intA, _ := strconv.Atoi(strings.Join(s1, ""))
//	intB, _ := strconv.Atoi(strings.Join(s2, ""))
//
//	val := fmt.Sprintf("%v", intA+intB)
//	vals := strings.Split(val, "")
//	for i, j := 0, len(s1)-1; i < j; i, j = i+1, j-1 {
//		vals[i], vals[j] = vals[j], vals[i]
//	}
//
//	intInit, _ := strconv.Atoi(vals[0])
//	res = ListNode{Val: intInit}
//	for i := 1; i < len(vals); i++ {
//		v := 0
//		if vals[i] != "0" {
//			v, _ = strconv.Atoi(vals[i])
//		}
//
//		res.Next = &ListNode{Val: -1}
//		fmt.Println("AAAA: ",&res.Next)
//		setData(res.Next, v)
//	}
//
//	return &res
//}
//
//func setData(res *ListNode, val int) {
//	fmt.Println("AAAA: ",&res)
//	if res == nil {
//		res = &ListNode{Val: val}
//		fmt.Println("test hihi: ", res)
//		return
//	}
//
//	fmt.Println("res hihi: ", res)
//	fmt.Println("val hihi: ", val)
//
//	if res.Next != nil {
//		res.Val = val
//	} else {
//		setData(res.Next, val)
//		//res.Next = &ListNode{Val: val}
//	}
//}
