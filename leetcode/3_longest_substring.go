package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "pwwkexwx"
	fmt.Println("input: ", s[:1], strings.LastIndex(s[:5], "w"))
	fmt.Println("result: ", lengthOfLongestSubstring(s))
}

func lengthOfLongestSubstring(s string) int { // pwwkew
	var highestCount, distance, prevPos int
	prevPos = -1
	for i, v := range s {
		c := string(v)
		pos := strings.LastIndex(s[:i], c)
		if pos > prevPos {
			prevPos = pos
		}
		distance = i - prevPos
		if distance>highestCount {
			highestCount = distance
		}
	}
	return highestCount
}

//func lengthOfLongestSubstring(s string) int {
//	var max, tmp int
//
//	tMap := make(map[string]int)
//	for i := 0; i < len(s); i++ {
//		_, ok := tMap[s[i:i+1]]
//		if ok {
//			tmp = 1
//			tMap[s[i:i+1]]++
//		} else {
//			tMap[s[i:i+1]] = 1
//			tmp++
//		}
//
//		if tmp > max {
//			max = tmp
//		}
//	}
//
//	return max
//}
