package main

import "fmt"

func main() {
	fmt.Println("res: ", recFactorial(4))
}

func recFactorial(n uint64) uint64 {
	if n == 0 {
		return 1
	}

	return n * recFactorial(n-1)
}
