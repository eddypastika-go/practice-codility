package main

import (
	"fmt"
)

func main() {
	A := []int{7, 3, 7, 3, 1, 3, 4, 1}
	fmt.Println("input: ", A)
	fmt.Println("result: ", shortestVacation(A))
}

func shortestVacation(A []int) int {
	newMap := make(map[int]int)
	for _, v := range A {
		newMap[v] = 0
	}

	count := avoidDuplicate(newMap, A)

	return count
}

func avoidDuplicate(m map[int]int, A []int) int {
	count := 0
	for k, v := range m {
		count = 0
		v = 0
		for i := 0; i < len(A); i++ {
			if k == A[i] {
				count++
				v++
				if v > 1 {
					v = 0
					avoidDuplicate(m, A[i:])
				}
				continue
			}
		}
	}

	return count
}
