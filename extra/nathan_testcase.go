package main

import (
	"fmt"
	"strings"
	"unicode"
)

func main()  {
	T := []string{"test1a, test2", "test1b", "test1c", "test3"}
	R := []string{"W", "OK", "R", "OK", "T"}
	fmt.Println("result: ", calPoints(T, R))
}

func calPoints(T []string, R []string) int {
	newMap := make(map[string]string)
	for i := 0; i < len(R); i++ {
		if strings.ToUpper(R[i]) == "OK" {
			newMap[T[i]] = R[i]
		}
	}

	value := 0
	for k, _ := range newMap {
		for _, r := range k[len(k)-1:] {
			if !unicode.IsLetter(r) {
				value += 100
			}
		}
	}

	return value
}
