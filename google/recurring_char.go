package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(findRecurring("CABCA"))
}

func findRecurring(s string) string {
	listString := strings.Split(s, "")

	newMap := make(map[string]int)
	for _, v := range listString {
		_, ok := newMap[v]
		if ok {
			return v
		} else {
			newMap[v] = 1
		}
	}

	return ""
}
