package main

import "fmt"

func main() {
	arrInt := []int{1, 2, 4, 4}
	sum := 6

	fmt.Println(hasPairSum(arrInt, sum))
	fmt.Println(hasPairSum2(arrInt, sum))

}

func hasPairSum(A []int, sum int) string {
	low, high := 0, len(A)-1

	for low < high {
		tmp := A[low] + A[high]
		if tmp < sum {
			low++
		} else if tmp > sum {
			high--
		} else if tmp == sum {
			return "YES"
		}
	}

	return "NO"
}

func hasPairSum2(A []int, sum int) string {
	mapComp := make(map[int]int)
	for _, v := range A {
		comp := sum - v
		_, ok := mapComp[comp]
		if ok {
			return "YES"
		} else {
			mapComp[v] = comp
		}
	}

	return "NO"
}
