package main

import "fmt"

func main() {
	fmt.Println(countDiv(6, 11, 2))
	fmt.Println(countDiv2(6, 11, 2))
	fmt.Println(countDiv2(1, 20, 2))

}

func countDiv(A, B, K int) int {
	count := 0
	for i := A; i <= B; i++ {
		if i%K == 0 {
			count++
		}
	}

	return count
}

func countDiv2(A, B, K int) int { // use this 0(1)
	if A%K == 0 {
		return (B-A)/K + 1
	}

	return (B - (A - A%K)) / K
}

