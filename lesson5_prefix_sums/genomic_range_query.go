package main

import (
	"fmt"
	"strings"
)

func main() {
	S := "CAGCCTA"
	P := []int{2, 5, 0}
	Q := []int{4, 5, 6}

	fmt.Println(findMinimumImpact(S, P, Q))
	fmt.Println(findMinimumImpact2(S, P, Q))
}

var DNA = map[string]int{
	"A": 1,
	"C": 2,
	"G": 3,
	"T": 4,
}

func findMinimumImpact(S string, P, Q []int) []int { // 62% lol
	arrDNA := strings.Split(S, "")

	var arrImpact []int
	for i := 0; i < len(P); i++ {
		startIdx := P[i]
		endIdx := Q[i] + 1
		newDNA := arrDNA[startIdx:endIdx]
		minImpact := DNA[newDNA[0]]
		for j := 1; j < len(newDNA); j++ {
			if minImpact > DNA[newDNA[j]] {
				minImpact = DNA[newDNA[j]]
			}
		}

		arrImpact = append(arrImpact, minImpact)

	}

	return arrImpact
}

// S := "CAGCCTA"
func findMinimumImpact2(S string, P, Q []int) []int { // with prefix sums method
	var a, c, g int
	genomes := make([][]int, 3)
	for i := range genomes {
		genomes[i] = make([]int, len(S)+1)
	}

	for i := 0; i < len(S); i++ {
		a, c, g = 0, 0, 0

		if "A" == S[i:i+1] {
			a = 1
		}
		if "C" == S[i:i+1] {
			c = 1
		}
		if "G" == S[i:i+1] {
			g = 1
		}

		// calculate prefix sums
		genomes[0][i+1] = genomes[0][i] + a
		genomes[1][i+1] = genomes[1][i] + c
		genomes[2][i+1] = genomes[2][i] + g
	}

	result := make([]int, len(P))
	for i := 0; i < len(P); i++ {
		startIdx := P[i]
		endIdx := Q[i] + 1

		if genomes[0][endIdx]-genomes[0][startIdx] > 0 {
			result[i] = 1
		} else if genomes[1][endIdx]-genomes[1][startIdx] > 0 {
			result[i] = 2
		} else if genomes[2][endIdx]-genomes[2][startIdx] > 0 {
			result[i] = 3
		} else {
			result[i] = 4
		}
	}

	return result
}
