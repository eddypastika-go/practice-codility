package main

import "fmt"

func main() {
	arrInt := []int{2, 1, 5, 6, 4}
	fmt.Println("result: ", findMissingElem(arrInt))

	act := sum(arrInt)
	exp := sumFullArr(len(arrInt) + 1)
	fmt.Println("result 2: ", exp-act)
}

func findMissingElem(A []int) (res int) {
	A = sort(A)
	for i := len(A) - 1; i > 0; i-- {
		if A[i]-A[i-1] > 1 {
			res = A[i] - 1
		}
	}

	return
}

func sort(A []int) []int {
	for i := 0; i < len(A); i++ {
		for j := 0; j < len(A); j++ {
			if A[i] < A[j] {
				tmp := A[i]
				A[i] = A[j]
				A[j] = tmp
			}
		}
	}

	return A
}

// ================================== use this
func sum(A []int) (res int) {
	for _, v := range A {
		res += v
	}

	return
}

func sumFullArr(n int) (res int) {
	for i := 1; i <= n; i++ {
		res += i
	}

	return
}
