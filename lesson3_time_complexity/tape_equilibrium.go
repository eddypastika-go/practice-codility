package main

import (
	"fmt"
	"math"
)

var arr []int

func main() {
	arrInt := []int{3, 1, 2, 4, 3, -3}

	fmt.Println("input: ", arrInt)
	findMinDiff(arrInt, 1)
	fmt.Println("arr: ", arr)
	fmt.Println("result: ", findMin(arr))
	fmt.Println("result 2: ", Solution(arrInt))
}

func findMinDiff(A []int, P int) {
	if P >= len(A) {
		return
	}

	arr1 := A[:P]
	arr2 := A[P:]

	val1 := sumArr(arr1)
	val2 := sumArr(arr2)

	total := val1 - val2
	arr = append(arr, int(math.Abs(float64(total))))

	findMinDiff(A, P+1)
}

func sumArr(A []int) (res int) {
	for _, v := range A {
		res += v
	}

	return
}

func findMin(A []int) (res int) {
	res = A[0]
	for _, v := range A {
		if v < res {
			res = v
		}
	}
	return
}

func Solution(A []int) int { // use this (resTemp = curSum - (fixSum - curSum))
	var fixSum, curSum, resTemp, valReturn int
	for i := 0; i < len(A); i++ { // sum
		fixSum += A[i]
		if A[i] < 0 {
			valReturn += ^A[i] + 1
		} else {
			valReturn += A[i]
		}
	}

	for i := 0; i < len(A)-1; i++ { // formula
		curSum += A[i]
		resTemp = curSum - (fixSum - curSum)
		if resTemp < 0 {
			resTemp = ^resTemp + 1
		}
		if resTemp < valReturn {
			valReturn = resTemp
		}
	}

	return valReturn
}
