package main

import "fmt"

func main() {
	fmt.Println("result: ", calJump(10, 120, 30))
	fmt.Println("result 2: ", calJump2(10, 120, 30))
}

func calJumpRecursive(x, y, d, jumpTime int) int {
	if (jumpTime*d)+x >= y {
		return jumpTime
	}

	return calJumpRecursive(x, y, d, jumpTime+1)
}

func calJump(x, y, d int) (res int) {
	if x == y {
		return 0
	}

	for {
		if (res*d)+x < y {
			res++
		} else {
			break
		}
	}

	return res
}

func calJump2(X, Y, D int) int { // use this for better performance
	result := (Y - X) / D

	if (Y-X)%D > 0 {
		result++
	}
	return result
}
