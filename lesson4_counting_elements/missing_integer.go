package main

import (
	"fmt"
	"sort"
)

func main() {
	A := []int{1, 3, 6, 4, 1, 2}
	//B := []int{1, 2, 3}

	fmt.Println("input: ", A)
	fmt.Println("result: ", findMissingInteger(A))
}

func findMissingInteger(A []int) (res int) {
	sort.Ints(A)
	res = 1
	for _, v := range A {
		if v == res {
			res++
		}
	}

	return
}
