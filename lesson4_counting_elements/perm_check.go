package main

import (
	"fmt"
	"sort"
)

func main() {
	A := []int{1, 1}
	//B := []int{1, 2, 3}

	fmt.Println("input: ", A)
	fmt.Println("result: ", permCheck(A))
}

func permCheck(A []int) int {
	sort.Ints(A)
	min := 1
	for i := 0; i < len(A); i++ {
		if min == A[i] {
			min++
		}

		if i > 0 && A[i] == A[i-1] {
			min--
		}
	}

	if min == A[len(A)-1]+1 {
		return 1
	}

	return 0
}
