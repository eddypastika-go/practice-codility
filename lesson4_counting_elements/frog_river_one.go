package main

import "fmt"

func main() {
	arrInt := []int{1, 3, 1, 1, 2, 3, 5, 4}
	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", findEarliestElem(2, arrInt))

}

func findEarliestElem(X int, A []int) int {
	elemMap := make(map[int]int)
	for i, v := range A {
		if v <= X {
			_, ok := elemMap[v]
			if ok {
				elemMap[v] += 1
			} else {
				elemMap[v] = 1
			}
		}

		if len(elemMap) == X {
			return i
		}
	}

	return -1
}
