package main

import (
	"fmt"
	"math"
)

func main() {
	arrInt := []int{3, 4, 4, 6, 1, 4, 4}
	fmt.Println("input: ", arrInt)
	fmt.Println("result: ", counter(5, arrInt))
	fmt.Println("result 2: ", counter2(5, arrInt))
}

func counter(N int, A []int) []int {
	counterArr := make([]int, N)
	for i := 0; i < N; i++ {
		counterArr[i] = 0
	}

	maxVal := 0
	for _, v := range A {
		if 1 <= v && v <= N {
			counterArr[v-1] += 1
			maxVal = counterArr[v-1]
		} else {
			for i := range counterArr {
				counterArr[i] = maxVal
			}
		}
	}

	return counterArr
}

func counter2(N int, A []int) []int { // use this
	currMax, lastIncrease := 0, 0
	counterArr := make([]int, N)
	for i := 0; i < N; i++ {
		counterArr[i] = 0
	}

	for _, v := range A {
		if v > N {
			lastIncrease = currMax
		} else {
			counterArr[v-1] = int(math.Max(float64(counterArr[v-1]), float64(lastIncrease)))
			counterArr[v-1]++
			currMax = int(math.Max(float64(currMax), float64(counterArr[v-1])))
		}
	}

	for i := 0; i < N; i++ {
		counterArr[i] = int(math.Max(float64(counterArr[i]), float64(lastIncrease)))
	}

	return counterArr
}
